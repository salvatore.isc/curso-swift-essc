//
//  VCLogin.swift
//  Curso Swift EssC
//
//  Created by Salvador Lopez on 12/06/23.
//

import UIKit

class VCLogin: UIViewController {
    
    var texto: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        print("1. viewDidLoad [VCLogin]")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("2. viewWillAppear [VCLogin]")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("3. viewDidAppear [VCLogin]")
        print(texto)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("4. viewWillDisappear [VCLogin]")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("5. viewDidDisappear [VCLogin]")
    }
}
