//
//  Bebida.swift
//  Curso Swift EssC
//
//  Created by Salvador Lopez on 15/06/23.
//

import Foundation

class Bebida {
    var nombre: String
    var precio: Int
    var promo: Bool
    
    init(nombre: String, precio: Int = 10, promo: Bool = false) {
        self.nombre = nombre
        self.precio = precio
        self.promo = promo
    }
    
}
