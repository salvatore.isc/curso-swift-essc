//
//  VCRegistro2.swift
//  Curso Swift EssC
//
//  Created by Salvador Lopez on 14/06/23.
//

import UIKit

class VCRegistro2: UIViewController {
    
    let colorsNames = ["white","gray","orange","blue","green"]
    let colors = [UIColor.white,UIColor.gray,UIColor.orange,UIColor.blue,UIColor.green]
    
    let wScreen = UIScreen.main.bounds.width
    let hScreen = UIScreen.main.bounds.height

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: PickerView
        let myPickerView = UIPickerView()
        myPickerView.frame = CGRect(x: 0, y: 100, width: wScreen, height: 260)
        myPickerView.delegate = self
        myPickerView.dataSource = self
        self.view.addSubview(myPickerView)
    }

}

extension VCRegistro2: UIPickerViewDelegate{
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return colorsNames[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("selected: \(component)(componente), \(row)(row).")
        print(colorsNames[row])
        self.view.backgroundColor = colors[row]
    }
    
}

extension VCRegistro2: UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return colorsNames.count
    }
    
}
