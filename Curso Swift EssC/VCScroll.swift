//
//  VCScroll.swift
//  Curso Swift EssC
//
//  Created by Salvador Lopez on 15/06/23.
//

import UIKit

class VCScroll: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var bebidas = [
        Bebida(nombre: "Expresso"),
        Bebida(nombre: "Expresso Doble"),
        Bebida(nombre: "Expresso Cortado", precio: 12, promo: true),
        Bebida(nombre: "Expresso con crema", precio: 13),
        Bebida(nombre: "Latte", promo: true),
        Bebida(nombre: "Té verde", precio: 9),
        Bebida(nombre: "Té negro", precio: 9),
        Bebida(nombre: "Chocolate", precio: 12, promo: true)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    /*func initScroll(){
        //MARK: UIScrollView
        let rect = self.view.bounds // Obteniendo Tamaño y Origen de Vista principal
        let scrollView = UIScrollView(frame: rect)
        let img = UIImage(named: "colorful")
        let imgView = UIImageView(image: img)
        scrollView.addSubview(imgView)
        self.view.addSubview(scrollView)
        scrollView.contentSize = img!.size
        scrollView.contentInset = UIEdgeInsets(top: -55, left: 50, bottom: -55, right: 50)
        scrollView.contentOffset = .init(x: 800, y: 800)
        scrollView.scrollIndicatorInsets = UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30)
    }*/

}

extension VCScroll: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bebidas.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellRightDetail = tableView.dequeueReusableCell(withIdentifier: "cellRightDetail", for: indexPath)
        cellRightDetail.textLabel?.text = bebidas[indexPath.row].nombre
        cellRightDetail.detailTextLabel?.text = "\(bebidas[indexPath.row].precio)"
        
        if bebidas[indexPath.row].promo {
            cellRightDetail.backgroundColor = .black
            cellRightDetail.textLabel?.textColor = .white
            cellRightDetail.detailTextLabel?.textColor = .white
        }
        
        return cellRightDetail
    }
    
    
}

extension VCScroll: UITableViewDelegate {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Menu de bebidas"
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Seccion: \(indexPath.section), Row: \(indexPath.row)")
        print("Bebida seleccionada: \(bebidas[indexPath.row].nombre)")
        
        if bebidas[indexPath.row].promo {
            
            //MARK: UIAlertController
            let alertController = UIAlertController(title: "¡Promocion de esta semana!", message: "Compra 2 \(bebidas[indexPath.row].nombre) y te regalamos el tercero gratis!", preferredStyle: .alert)
            
            let btnOk = UIAlertAction(title: "¡Acepto!", style: .default){
                _ in
                print("Agregar 3 cafes y cobrar 2")
            }
            let btnCancel = UIAlertAction(title: "No por ahora", style: .cancel)
            
            alertController.addAction(btnCancel)
            alertController.addAction(btnOk)
            
            /*alertController.addTextField{
                textF in
                textF.placeholder = "Ingresa un código de promoción:"
            }*/
            
            self.present(alertController, animated: true)
            
        }
        
    }
}
