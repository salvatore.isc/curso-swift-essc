//
//  ViewController.swift
//  Curso Swift EssC
//
//  Created by Salvador Lopez on 12/06/23.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var usuarioTextField: UITextField!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var forgotPassBtn: UIButton!
    
    var wScreen = UIScreen.main.bounds.width
    
    @IBAction func loginAction(_ sender: UIButton) {
        //Instrucciones del IBAction - TouchUpInside
        print("Login...")
        print(usuarioTextField.text)
        print(passTextField.text)
    }
    
    @IBAction func forgotPassword(_ sender: Any) {
        print("Olvide mi contraseña")
    }
    
    @IBAction func registerAction(_ sender: Any) {
        print("Registro")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        registerBtn.backgroundColor = .red
        registerBtn.tintColor = .white
        registerBtn.layer.cornerRadius = 8
        print("1. viewDidLoad [VC]")
        
        //MARK: SetUP UIView.animate()
        self.view.backgroundColor = .white
        self.usuarioTextField.frame.origin.x = -wScreen
        self.passTextField.frame.origin.x = -wScreen * 2
        self.imgView.frame.origin.x = -wScreen * 0.5
        self.welcomeLabel.frame.origin.y = -100
        self.imgView.alpha = 0
        self.registerBtn.alpha = 0
        self.loginBtn.alpha = 0
        self.forgotPassBtn.alpha = 0
 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("2. viewWillAppear [VC]")
        UIView.animate(withDuration: 1, delay: 1) {
        
            self.view.backgroundColor = .systemGray5
            self.usuarioTextField.frame.origin.x = 84
            self.passTextField.frame.origin.x = 84
            self.imgView.frame.origin.x = 76
            self.welcomeLabel.frame.origin.y = 103
            
        }
        UIView.animate(withDuration: 2, delay: 0.8, animations:  {
            self.imgView.alpha = 1
        }) { _ in
            self.registerBtn.alpha = 1
            self.loginBtn.alpha = 1
            self.forgotPassBtn.alpha = 1
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("3. viewDidAppear [VC]")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("4. viewWillDisappear [VC]")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("5. viewDidDisappear [VC]")
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        let vcd = segue.destination as? VCLogin
        vcd?.texto = "Hola desde el VC origen"
    }

}

