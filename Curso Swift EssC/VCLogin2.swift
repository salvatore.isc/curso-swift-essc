//
//  VCLogin2.swift
//  Curso Swift EssC
//
//  Created by Salvador Lopez on 14/06/23.
//

import UIKit

class VCLogin2: UIViewController {
    
    let textField = UITextField()
    let myActivityIndicator = UIActivityIndicatorView()
    
    //PROPERTIES UIPROGRESSVIEW
    var seconds: Float = 0
    var timer: Timer!
    var myPreogressView: UIProgressView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: UILABEL
        
        let label = UILabel(frame: CGRect(x: 85, y: 50, width: 200, height: 50))
        label.textAlignment = .center
        label.text = "Swift Login"
        //label.layer.borderWidth = 1
        //label.layer.borderColor = CGColor(red: 230/255, green: 176/255, blue: 170/255, alpha: 0.5)
        label.textColor = UIColor.blue
        self.view.addSubview(label)
        
        //MARK: UIBUTTON
        
        let button = UIButton()
        button.frame = CGRect(x: 85, y: 160, width: 200, height: 50)
        button.backgroundColor = .lightGray
        button.setTitle("Enviar", for: .normal)
        button.addTarget(self, action: #selector(btnAction), for: .touchUpInside)
        self.view.addSubview(button)
        
        //MARK: UITEXTFIELD
        
        
        textField.frame = CGRect(x: 85, y: 105, width: 200, height: 50)
        textField.textAlignment = .center
        textField.textColor = .gray
        //textField.borderStyle = .line
        textField.placeholder = "Ingresa tu numbre de usuario"
        textField.isSecureTextEntry = true
        //textField.layer.borderColor = UIColor.blue.cgColor
        //textField.layer.borderWidth = 1
        self.view.addSubview(textField)
        
        //MARK: UIIMAGEVIEW
        
        let image = UIImage(named: "tux")
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: 160, y: 215, width: 50, height: 50)
        imageView.contentMode = .scaleAspectFit
        self.view.addSubview(imageView)
        
        //MARK: UITextView
        
        let str: NSString = "Mauris dolor elit, tincidunt in velit ut, maximus bibendum tortor. Cras id augue pellentesque, semper est at, convallis lectus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam non molestie magna. Nullam in sapien ante. Donec ullamcorper tincidunt euismod. Nullam eu mauris id nibh dignissim pretium ac sodales diam. Phasellus sit amet pretium quam. Aenean bibendum eleifend nulla eget malesuada."
        
        let textView = UITextView(frame: CGRect(x: 15, y: 280, width: 350, height: 50))
        textView.text = str as String
        textView.isSelectable = false
        textView.isEditable = false
        self.view.addSubview(textView)
        
        //MARK: UITextView 2
        
        let myBoldWord = str.range(of: "Mauris dolor")
        
        let myMutableString = NSMutableAttributedString(string: str as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Georgia", size: 18)!])
        
        myMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Helvetica Neue", size: 32)!, range: myBoldWord)
        
        let textView2 = UITextView(frame: CGRect(x: 15, y: 355, width: 350, height: 50))
        textView2.attributedText = myMutableString
        
        //self.view.addSubview(textView2)
        
        //MARK: UISlider
        
        let mySlider = UISlider(frame: CGRect(x: 15, y: 410, width: 280, height: 20))
        mySlider.minimumValue = 0
        mySlider.maximumValue = 10
        mySlider.isContinuous = true
        mySlider.tintColor = .magenta
        mySlider.value = 5
        mySlider.addTarget(self, action: #selector(sliderAction(sender:)), for: .valueChanged)
        //self.view.addSubview(mySlider)
        
        //MARK: UISwitch
        
        let mySwitch = UISwitch(frame: CGRect(x: 15, y: 475, width: 20, height: 20))
        mySwitch.isOn =  true
        mySwitch.addTarget(self, action: #selector(switchAction(sender:)), for: .valueChanged)
        self.view.addSubview(mySwitch)
        
        //MARK: UIActivityIndicator
        myActivityIndicator.style = .large
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.startAnimating()
        //self.view.addSubview(myActivityIndicator)
        
        //MARK: UIProgressView
        myPreogressView = UIProgressView(progressViewStyle: .default)
        myPreogressView.frame.size.width = 300
        myPreogressView.center = self.view.center
        myPreogressView.progress = 0
        myPreogressView.trackTintColor = .gray
        myPreogressView.progressTintColor = .green
        self.view.addSubview(myPreogressView)
        //exTimer()
        
        //MARK: UIStepper
        let myStepper = UIStepper()
        myStepper.frame.origin = CGPoint(x:15, y:370)
        myStepper.maximumValue = 10
        myStepper.minimumValue = 0
        myStepper.stepValue = 2
        myStepper.autorepeat = true
        myStepper.wraps = true
        myStepper.value = 0
        myStepper.addTarget(self, action: #selector(updateValueStepper(sender:)), for: .valueChanged)
        self.view.addSubview(myStepper)
        
        //MARK: UIDatePicker
        let datePicker = UIDatePicker()
        datePicker.center = self.view.center
        datePicker.datePickerMode = .date
        datePicker.backgroundColor = .lightGray
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.addTarget(self, action: #selector(updateDatePicker(sender:)), for: .valueChanged)
        self.view.addSubview(datePicker)
        
    
    }
    
    @objc func updateDatePicker(sender:UIDatePicker){
        print(sender.date)
        let format = DateFormatter()
        format.dateFormat = "EEE, MMM d ''yy"
        let dateFormated = format.string(from: sender.date)
        print(dateFormated)
    }
    
    @objc func updateValueStepper(sender: UIStepper){
        print(sender.value)
    }
    
    func exTimer(){
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(updateProgressView), userInfo: nil, repeats: true)
    }
    
    @objc func updateProgressView(){
        seconds += 1
        if seconds <= 10 {
            myPreogressView.setProgress(seconds/10, animated: true)
            print("\(seconds*10)%")
        }else{
            timer.invalidate()
        }
    }
    
    
    @objc func switchAction(sender:UISwitch){
        var color = sender.isOn ? UIColor.white : UIColor.black
        self.view.backgroundColor = color
    }
    
    @objc func btnAction(){
        print("button pressed!")
        
        myActivityIndicator.stopAnimating()
        if let valor = self.textField.text {
            print("El valor ingresado en el textField : \(valor)")
        }
    }
    
    @objc func sliderAction(sender:UISlider){
        print(sender.value)
    }

}
